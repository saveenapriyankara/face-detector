import cv2

#haarcascade only take grey-scale images

try:
     # Cascade    : algorithm    
     # # Classifier : detector
    trained_face_data = cv2.CascadeClassifier('C:/Users/16478/Desktop/Myprojects/haarcascade_frontalface_default.xml')
  
    #Choosing an image to detect faces - Importing an image
    #image = cv2.imread('people.jpg')
    webcam = cv2.VideoCapture(0)

    while True:
        if_frame_read_success, frame = webcam.read()

    #Convert to grey scale
        grayscaled_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
 
    # detectMultiscale : despite the face sizes
        face_coordinates = trained_face_data.detectMultiScale(grayscaled_image)
        print(face_coordinates)

    #cv2.rectangle((image, (x, y)))

        for (x, y, w, h) in face_coordinates:
        #(x, y, w, h) = face_coordinates[0]
            cv2.rectangle(frame,(x, y), (x+w, y+h), (0, 255, 0), 2 )

   
        cv2.imshow('greyscaled image', frame)

        #Adding 1 - will wait for 1 ms
        key = cv2.waitKey(2)
        
        #Q to break, uppercase or lowercase
        if key==81 or key==113:
            break
     

    #Pause the image - Just like sleep
        print("code worked!")




except Exception as e:
    print(e)    