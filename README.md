Useful Notes:

Link to Haarscade : 'https://github.com/opencv/opencv/tree/master/data/haarcascades'
Haarscade has trained AI models for years
Haarcascade only take grey-scale images

CascadeClassifier
    Cascade : algorithm
    Classifier : detector

First coordinate x,y is the top left coordinate
x+w --> face width
y+h --> face height
